# motorola moto g(20)

- manufacturer: motorola
- platform: ums512
- codename: java
- flavor: p352_Natv-user
- release: 11
- id: RTAS31.68-66-3
- incremental: 66-3
- tags: release-keys
- fingerprint: motorola/java_retail/java:11/RTAS31.68-66-3/66-3:user/release-keys
- is_ab: true
- brand: motorola